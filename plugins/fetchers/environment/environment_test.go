package environment

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.oit.duke.edu/devil-ops/devil-creds/devilcreds"
)

func TestGoodEnvironmentFetcher(t *testing.T) {
	opts := &EnvironmentOpts{
		RequiredVars: []string{"USERNAME", "PASSWORD"},
	}
	os.Clearenv()

	os.Setenv("USERNAME", "testuser")
	os.Setenv("PASSWORD", "testpassword")

	var f devilcreds.Fetcher
	f = &EnvironmentFetcher{}
	err := f.Validate(opts)
	require.NoError(t, err)
	creds, err := f.Fetch(opts)
	require.NoError(t, err)
	require.IsType(t, map[string]string{}, creds)

	// Make sure we get a lowercase key back
	require.Contains(t, creds, "username")
}

func TestMissingEnvironmentFetcher(t *testing.T) {
	opts := &EnvironmentOpts{
		RequiredVars: []string{"USERNAME", "PASSWORD"},
	}
	os.Clearenv()

	os.Setenv("USERNAME", "testuser")

	var f devilcreds.Fetcher
	f = &EnvironmentFetcher{}
	err := f.Validate(opts)
	require.Error(t, err)
	_, err = f.Fetch(opts)
	require.Error(t, err)
}
