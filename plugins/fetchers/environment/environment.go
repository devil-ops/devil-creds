package environment

import (
	"fmt"
	"os"
	"strings"

	"gitlab.oit.duke.edu/devil-ops/devil-creds/devilcreds"
)

type EnvironmentFetcher struct{}

type EnvironmentOpts struct {
	RequiredVars []string
}

func (f *EnvironmentFetcher) Validate(opt interface{}) error {
	missingVars := devilcreds.ListMissingEnvVars(opt.(*EnvironmentOpts).RequiredVars)
	if len(missingVars) > 0 {
		return fmt.Errorf("Missing env vars: %v", missingVars)
	}
	return nil
}

func (f *EnvironmentFetcher) Fetch(opt interface{}) (map[string]string, error) {
	err := f.Validate(opt)
	if err != nil {
		return nil, err
	}
	ret := make(map[string]string)
	for _, v := range opt.(*EnvironmentOpts).RequiredVars {
		ret[strings.ToLower(v)] = os.Getenv(v)
	}
	return ret, nil
}
