package vault

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.oit.duke.edu/devil-ops/devil-creds/devilcreds"
)

func TestGoodVaultFetcher(t *testing.T) {
	opts := &VaultOpts{
		KVPath: "secret/data/test",
		Keys:   []string{"password"},
	}
	os.Clearenv()

	os.Setenv("VAULT_ADDR", "http://localhost:8200")
	os.Setenv("VAULT_TOKEN", "foo")

	var f devilcreds.Fetcher
	f = &VaultFetcher{}
	err := f.Validate(opts)
	require.NoError(t, err)
	/*creds, err := f.Fetch(opts)
	require.NoError(t, err)
	require.IsType(t, map[string]string{}, creds)*/
}

func TestMissingEnvVaultFetcher(t *testing.T) {
	opts := &VaultOpts{
		KVPath: "secret/data/test",
		Keys:   []string{"password"},
	}
	os.Clearenv()

	os.Setenv("VAULT_ADDR", "http://localhost:8200")

	var f devilcreds.Fetcher
	f = &VaultFetcher{}
	err := f.Validate(opts)
	require.Error(t, err)
	_, err = f.Fetch(opts)
	require.Error(t, err)
}
