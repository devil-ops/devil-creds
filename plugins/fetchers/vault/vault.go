package vault

import (
	"errors"
	"fmt"

	hvault "github.com/hashicorp/vault/api"
	"gitlab.oit.duke.edu/devil-ops/devil-creds/devilcreds"
)

type VaultFetcher struct{}

type VaultOpts struct {
	// Path of the secret
	KVPath string
	// Which keys to return
	Keys []string
}

// To start, require VAULT_ADDR and VAULT_TOKEN to be set. We may wanna remove
// VAULT_TOKEN later, and also check for a token file
var reqVars = []string{"VAULT_ADDR", "VAULT_TOKEN"}

func (f *VaultFetcher) Validate(opt interface{}) error {
	missingVars := devilcreds.ListMissingEnvVars(reqVars)
	if len(missingVars) > 0 {
		return fmt.Errorf("Missing env vars: %v", missingVars)
	}
	if len(opt.(*VaultOpts).Keys) == 0 {
		return errors.New("Must specify at least 1 key in the secret to retrieve")
	}
	return nil
}

func (f *VaultFetcher) Fetch(opt interface{}) (map[string]string, error) {
	err := f.Validate(opt)
	if err != nil {
		return nil, err
	}
	config := hvault.DefaultConfig() // modify for more granular configuration
	client, err := hvault.NewClient(config)
	if err != nil {
		return nil, err
	}

	v, err := client.Logical().Read(opt.(*VaultOpts).KVPath)
	if err != nil {
		return nil, err
	}
	ret := make(map[string]string)
	if v == nil {
		return nil, errors.New("No data returned from vault. Is the path correct?")
	}
	d := v.Data["data"].(map[string]interface{})
	if d == nil {
		return nil, errors.New("Got data, but not subdata...sorry for the awful description 😭")
	}
	for _, wantedKey := range opt.(*VaultOpts).Keys {
		if val, ok := d[wantedKey]; ok {
			ret[wantedKey] = val.(string)
		} else {
			return nil, fmt.Errorf("Key not found in vault: %v", wantedKey)
		}
	}
	return ret, nil
}
