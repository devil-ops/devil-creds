package main

import (
	"log"
	"os"

	"gitlab.oit.duke.edu/devil-ops/devil-creds/devilcreds"
	"gitlab.oit.duke.edu/devil-ops/devil-creds/plugins/fetchers/vault"
)

func main() {
	var f devilcreds.Fetcher

	if len(os.Args) != 2 {
		log.Fatalf("Usage: %s kv_path", os.Args[0])
	}
	f = &vault.VaultFetcher{}
	opts := &vault.VaultOpts{
		KVPath: os.Args[1],
		Keys:   []string{"password"},
	}
	d, err := f.Fetch(opts)
	if err != nil {
		log.Fatalf("Error fetching: %v", err)
	}
	log.Printf("%+v\n", d)
}
