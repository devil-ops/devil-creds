package main

import (
	"log"

	"gitlab.oit.duke.edu/devil-ops/devil-creds/devilcreds"
	"gitlab.oit.duke.edu/devil-ops/devil-creds/plugins/fetchers/environment"
)

func main() {
	var f devilcreds.Fetcher

	f = &environment.EnvironmentFetcher{}
	opts := &environment.EnvironmentOpts{
		RequiredVars: []string{"DEVIL_CREDS_ENV_VAR_1", "DEVIL_CREDS_ENV_VAR_2"},
	}
	log.Println(f.Validate(opts))
	log.Println(f.Fetch(opts))
}
