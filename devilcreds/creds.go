package devilcreds

// Fetcher interface should be implemented by any struct that can fetch and validate
type Fetcher interface {
	Validate(opt interface{}) error
	Fetch(opt interface{}) (map[string]string, error)
}
