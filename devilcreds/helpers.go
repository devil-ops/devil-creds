package devilcreds

import "os"

// ListMissingEnvVars returns a list of missing environment variables
func ListMissingEnvVars(requiredVars []string) []string {
	missingVars := make([]string, 0)
	for _, v := range requiredVars {
		if _, ok := os.LookupEnv(v); !ok {
			missingVars = append(missingVars, v)
		}
	}
	return missingVars
}
