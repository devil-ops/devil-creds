# Devil Creds

Module allowing for a standardised way of storing and retrieving credentials,
compatible with popular Duke tooling.

This is meant to be used in other Duke go projects to handle credential retrievals
